# Saas登陆功能API文档

![](/Users/wangyushu/Downloads/0zm0w12000dgjbwfdE5A1.png)

## 注册 /api/login/register

request：

```json
{
    "email":"384623032@qq.com",
    "password":"123456"
}
```

注册功能，注册成功后会往目标email发一封邮件，需要用户进邮件验证方可生效

## 校验 /api/login/validate

request: 同【注册】

在用户注册输入email时做校验，校验邮箱是否被注册，格式是否正确

## 验证 /api/login/verify

request:

```json
{
    "eamil":"xxx@yy.com"
    "verifyCode":"506976"
}
```

用户点击邮件触发，需要给一个链接，用户点击邮箱链接自动触发，验证成功后会生成一个token(email)，用于bind

## 绑定 /api/login/bind

header需要带`verify`或是`oauthLogin`生成的accessToken，key:"access-token"

request:

```json
{
    "email":"xxx@yy.com"
    "tenantName":"trip"
}
```

绑定成功后，会激活用户，并为其生成数据库，将用户信息和companySeed写入其中



## 登陆 /api/login/login

request:

```json
{
    "email":"xxx@yy.com"
    "password":"123456"
}
```

response:

```json
{
    "responseStatusType": {
        "responseCode": 0,
        "responseDesc": "success",
        "timestamp": 1710840482877
    },
    "body": {
        "success": true,
        "accessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MTE0NDUyODIsInVzZXJuYW1lIjoid2lsZGUifQ.6zUoIZIstpS7PAd5kAMADoc5wSG0XV2s2yEiTbfzipg"
    }
}
```



## 鉴权登陆

/api/login/oauthLogin

request:

```json
{
    "oauthType":1,
    "redirectUri":"xxx",
    "code":"4/0AeaYSHCCEbO-WPtpKnqMU91QunYa9CtIaG3QabYOSaurmexyJvrQi3TB0hXd8izIMkibHQ",
}
```

response：

```json
{
    "needBind":true,
    "email":"xxx@yy.com",
    "accessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MTE0NDUyODIsInVzZXJuYW1lIjoid2lsZGUifQ.6zUoIZIstpS7PAd5kAMADoc5wSG0XV2s2yEiTbfzipg"
}
```

对于已经成功激活的用户，直接登陆，返回的token由tenantName加密；

如果不存在可用用户，引导用户进入bind页面，返回由email加密的token，目前只可用于bind
