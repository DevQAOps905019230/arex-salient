FROM node:lts

ARG GITHUB_SHA
ARG GITHUB_REF

ENV GITHUB_SHA=${GITHUB_SHA}
ENV GITHUB_REF=${GITHUB_REF}

RUN echo "GITHUB_SHA=${GITHUB_SHA}"
RUN echo "GITHUB_REF=${GITHUB_REF}"

MAINTAINER wr_zhang25

RUN mkdir -p /app
COPY . /app/
WORKDIR /app

RUN npm install pnpm -g
RUN pnpm i
RUN pnpm run build

EXPOSE 8080
CMD ["node", "server.js" ]
