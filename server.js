import { createProxyMiddleware } from 'http-proxy-middleware';
import history from 'connect-history-api-fallback';
import express from 'express';

const app = express();
const API_URL = process.env['API_URL']||`http://10.144.62.53:8094`;
app.use(
  '/api',
  createProxyMiddleware({
    target: API_URL,
    changeOrigin: true,
  }),
);


app.use(history());

app.use(express.static('dist'));

app.listen(8080, function () {
  console.log(`Server is running`);
});