import react from '@vitejs/plugin-react-swc';
import AutoImport from 'unplugin-auto-import/vite';
import AntdResolver from 'unplugin-auto-import-antd';
import { defineConfig } from 'vite';
import Canyon from 'vite-plugin-canyon';
import Pages from 'vite-plugin-pages';
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react({
      plugins: [['swc-plugin-coverage-instrument', {}]],
    }),
    AutoImport({
      imports: ['react'],
      dts: './src/auto-imports.d.ts',
      resolvers: [AntdResolver()],
    }),
    Pages({
      exclude: ['**/helper/**'],
    }),
    Canyon({
      dsn: 'https://app.canyoncov.com/coverage/client',
      reporter:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IndyX3poYW5nMjUiLCJpZCI6MTAxMjA1ODAsImlhdCI6MTcxODQzMjc1OSwiZXhwIjoyMDM0MDA4NzU5fQ.2C7hUfC_uVykgvr4y4cw3SW9BO5K187921IVdrFGY9c',
      commitSha: process.env.GITHUB_SHA,
      projectID: '59069994',
      branch: process.env.GITHUB_REF,
    }),
  ],
  server: {
    port: 8000,
    host: '0.0.0.0',
    proxy: {
      '^/api': {
        target: 'https://cloud.arextest.com',
        changeOrigin: true,
      },
    },
  },
});
