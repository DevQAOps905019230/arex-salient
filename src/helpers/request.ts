// 封装一个axios请求

import { message } from 'antd';
import axios from 'axios';

import { getToken } from './auth.ts';

const request = axios.create({
  // baseURL: process.env.REACT_APP_BASE_URL,
});

const errMap = {
  '1000': 'login failed',
  '1001': 'user existed',
  '1002': 'user not existed',
  '1003': 'send email failed',
  '1004': 'verify failed',
  '1005': 'password error',
  '1006': 'user binded',
  '1008': 'company code error',
  '1009': 'company code bound',
  '1011': 'init user error',
};

request.interceptors.request.use(
  (config) => {
    const token = getToken();
    if (token) {
      config.headers['access-token'] = `${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

request.interceptors.response.use(
  (response) => {
    if (response.data.body?.success || response.data.body?.user) {
      return response.data.body;
    } else {
      if (response.data.body?.errorCode) {
        message.error(
          errMap[response.data.body.errorCode as keyof typeof errMap],
        );
      }
      return Promise.reject(response.data?.body);
    }
  },
  (error) => {
    if (error.response) {
      message.error(error.response.data.message);
    }
    return Promise.reject(error);
  },
);

export default request;
