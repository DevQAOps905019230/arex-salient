import './index.css';
import 'antd/dist/reset.css';

import {
  ApolloClient,
  ApolloProvider,
  createHttpLink,
  InMemoryCache,
} from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { message } from 'antd';
// import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';

import App from './App.tsx';
// 创建一个error link来处理错误
const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.forEach(({ message: msg, locations, path }) => {
      console.error(
        `[GraphQL error]: msg: ${msg}, Location: ${locations}, Path: ${path}`,
      );
      message.error(`[GraphQL error]: msg: ${msg}, Path: ${path}`);
      if (
        msg === 'Unauthorized' &&
        window.location.pathname !== '/oauth' &&
        window.location.pathname !== '/login'
      ) {
        localStorage.clear();
        window.location.href = '/login';
      }
      // 在这里你可以执行自定义的操作，比如显示错误提示
    });
  }
  if (networkError) {
    console.error(`[Network error]: ${networkError}`);
    // 在这里你可以执行自定义的操作，比如显示网络错误提示
  }
});

// 创建一个http link来发送GraphQL请求
const httpLink = createHttpLink({
  uri: '/graphql', // 你的GraphQL API的URL

  headers: {
    Authorization: `Bearer ` + (localStorage.getItem('token') || ''),
  },
});
// 创建Apollo Client实例
const client = new ApolloClient({
  link: errorLink.concat(httpLink), // 将error link和http link组合起来
  cache: new InMemoryCache(),
});

import * as Sentry from '@sentry/react';

Sentry.init({
  dsn: 'https://385038db56d1aee29704f4c03d873a49@o1083554.ingest.us.sentry.io/4507248419209216',
  integrations: [
    Sentry.browserTracingIntegration(),
    Sentry.replayIntegration(),
  ],
  // Performance Monitoring
  tracesSampleRate: 1.0, //  Capture 100% of the transactions
  // Set 'tracePropagationTargets' to control for which URLs distributed tracing should be enabled
  tracePropagationTargets: ['localhost', /^https:\/\/yourserver\.io\/api/],
  // Session Replay
  replaysSessionSampleRate: 0.1, // This sets the sample rate at 10%. You may want to change it to 100% while in development and then sample at a lower rate in production.
  replaysOnErrorSampleRate: 1.0, // If you're not already sampling the entire session, change the sample rate to 100% when sampling sessions where errors occur.
});

ReactDOM.createRoot(document.getElementById('root')!).render(
  <BrowserRouter>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </BrowserRouter>,
);
