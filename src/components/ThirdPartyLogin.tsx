import googleSvg from '../assets/img/google.svg';
const ThirdPartyLogin = () => {
  const url = new URL('https://accounts.google.com/o/oauth2/auth');

  const params = {
    response_type: 'code',
    state: 'STATE',
    scope: 'https://www.googleapis.com/auth/userinfo.email',
    client_id:
      '321806507825-7ajin7m8v3bt0td6hg9bf8r2iulh4c70.apps.googleusercontent.com',
    redirect_uri: window.location.origin + '/oauth',
  };

  url.search = new URLSearchParams(params).toString();
  // const sss = url.toString();

  console.log(url.toString());

  // const client_id = `0440a53cf60be5c587c7`;
  // const redirect_uri = `http://localhost:8000/apps/github/auth/callback`;
  // const scope = `user:email`;
  return (
    <div className={'flex justify-center'}>
      <a className={'cursor-pointer'} href={url.toString()}>
        <img src={googleSvg} className={'w-[24px] mr-2'} alt="" />
        <span>Login with Google</span>
      </a>
    </div>
  );
};

export default ThirdPartyLogin;
