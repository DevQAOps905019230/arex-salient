import { message } from 'antd';
import { useNavigate, useSearchParams } from 'react-router-dom';

import request from '../helpers/request.ts';

const Oauth = () => {
  const [URLSearchParams] = useSearchParams();
  const nav = useNavigate();
  request
    .post(`/api/login/oauthLogin`, {
      code: URLSearchParams.get('code'),
      redirectUri: window.location.origin + '/oauth',
      oauthType: 1,
    })
    .then((res: any) => {
      if (res.email && res.accessToken) {
        if (res.needBind) {
          localStorage.setItem('email', res.email);
          localStorage.setItem('sign-token', res.accessToken);
          nav('/auth/bind');
        } else {
          // 如果不需要绑定，直接登录
          localStorage.setItem('token', res.accessToken);
          nav('/');
        }
      } else {
        message.error('oauth failed');
      }
    });
  return <div>Oauth</div>;
};

export default Oauth;
