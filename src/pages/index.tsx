import Icon, {
  ArrowRightOutlined,
  FolderOutlined,
  LogoutOutlined,
} from '@ant-design/icons';
import { useRequest } from 'ahooks';
import { CanyonLayoutBase } from 'canyon-ui-old';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';

import UilUsersAlt from '../assets/users-icon.tsx';
import request from '../helpers/request.ts';
const { Text } = Typography;
import * as Sentry from '@sentry/react';
const IndexPage = () => {
  const loc = useLocation();
  const nav = useNavigate();
  const [menuSelectedKey, setMenuSelectedKey] = useState<string>('projects');
  const t = (c) => c;
  const meData = {
    me: {
      nickname: (localStorage.getItem('email') || '').split('@')[0],
      email: localStorage.getItem('email'),
    },
  };
  useEffect(() => {
    if (loc.pathname === '/') {
      nav('/dashboard');
    }
    try {
      // @ts-ignore
      fetch(window.__canyon__.dsn, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          // @ts-ignore
          Authorization: `Bearer ${window.__canyon__.reporter}`,
        },
        body: JSON.stringify({
          // @ts-ignore
          coverage: window.__coverage__,
          // @ts-ignore
          commitSha: window.__canyon__.commitSha,
          // @ts-ignore
          projectID: window.__canyon__.projectID,
          // @ts-ignore
          instrumentCwd: window.__canyon__.instrumentCwd,
          // @ts-ignore
          reportID: `${meData?.me.email}|${loc.pathname}`,
          // @ts-ignore
          branch: window.__canyon__.branch,
        }),
      });
    } catch (e) {
      // console.log(e);
    }
  }, [loc.pathname]);
  useEffect(() => {
    setMenuSelectedKey(loc.pathname.replace('/', ''));

    Sentry.setTag('user', localStorage.getItem('email') || 'anonymous');
  }, [loc.pathname]);

  useRequest(() => request.post(`/api/login/queryTenant`), {
    onSuccess(r: any) {
      // @ts-ignore
      window.userinfo = r.user;
      if (
        localStorage.getItem('email') &&
        localStorage.getItem('tenantCode') &&
        localStorage.getItem('tenantToken')
      ) {
        localStorage.setItem('email', r.user.email);
        localStorage.setItem('tenantCode', r.user.tenantCode);
        localStorage.setItem('tenantName', r.user.tenantName);
        localStorage.setItem(
          'tenantToken',
          r.user.tenantToken || 'tenantToken',
        );
      } else {
        localStorage.setItem('email', r.user.email);
        localStorage.setItem('tenantCode', r.user.tenantCode);
        localStorage.setItem('tenantName', r.user.tenantName);
        localStorage.setItem(
          'tenantToken',
          r.user.tenantToken || 'tenantToken',
        );

        window.location.href = `/`;
      }
    },
    onError(e) {
      localStorage.clear();
      nav(`/login`);
    },
  });
  return (
    <div>
      {localStorage.getItem('email') && (
        <CanyonLayoutBase
          footerName={'AREX'}
          itemsDropdown={[
            {
              label: (
                <div className={'text-red-500'}>
                  <LogoutOutlined className={'mr-2'} />
                  Logout
                </div>
              ),
              onClick: () => {
                localStorage.clear();
                window.location.href = '/login';
              },
            },
          ]}
          MeData={meData}
          onClickGlobalSearch={() => {}}
          title={
            <div>
              <span>AREX</span>
              <Text
                type={'secondary'}
                style={{ fontSize: '14px' }}
                className={'ml-2'}
              >
                {localStorage.getItem('tenantName')}
              </Text>
            </div>
          }
          logo={
            <div>
              <img src="/arex.png" alt="" className={'w-[30px]'} />
            </div>
          }
          mainTitleRightNode={
            <div>
              <Tooltip
                title={
                  <div>
                    <span>{t('menus.docs')}</span>
                    <ArrowRightOutlined />
                  </div>
                }
              >
                <a target={'_blank'} rel="noreferrer" className={'ml-2'}></a>
              </Tooltip>
            </div>
          }
          menuSelectedKey={menuSelectedKey}
          onSelectMenu={(selectInfo) => {
            console.log(selectInfo);
            setMenuSelectedKey(selectInfo.key);
            nav(`/${selectInfo.key}`);
          }}
          menuItems={[
            {
              label: t('Dashboard'),
              key: 'dashboard',
              icon: <FolderOutlined />,
            },
            // {
            //   label: t('Settings'),
            //   key: 'settings',
            //   icon: <SettingOutlined />,
            // },
            // {
            //   label: t('Usage'),
            //   key: 'usage',
            //   icon: <LineChartOutlined />,
            // },
            // {
            //   label: t('Billing'),
            //   key: 'billing',
            //   icon: <CreditCardOutlined />,
            // },
            {
              label: t('Members'),
              key: 'members',
              icon: (
                <Icon component={UilUsersAlt} style={{ fontSize: '15px' }} />
              ),
            },
          ]}
          renderMainContent={<Outlet />}
          search={false}
          account={false}
        />
      )}
    </div>
  );
};

export default IndexPage;
