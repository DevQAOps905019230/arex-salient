import { message } from 'antd';
import { useNavigate, useSearchParams } from 'react-router-dom';

import request from '../../helpers/request.ts';

const AuthAction = () => {
  const [URLSearchParams] = useSearchParams();
  const nav = useNavigate();
  useEffect(() => {
    request
      .post('/api/login/verify', {
        verifyCode: URLSearchParams.get('code'),
        email: URLSearchParams.get('email'),
      })
      .then((res: any) => {
        localStorage.setItem('sign-token', res.accessToken);
        message.success('verify success');
        nav('/auth/bind');
      })
  }, []);

  return <div>Authentication...</div>;
};

export default AuthAction;
