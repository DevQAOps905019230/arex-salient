// import {Button} from "antd";

import { useRequest } from 'ahooks';
import { message } from 'antd';

import request from '../../helpers/request.ts';

function checkValue(input) {
  // 使用正则表达式检查输入是否只包含数字和字母
  return /^[a-zA-Z0-9]+$/.test(input);
}
function replaceNonAlphanumeric(inputString) {
  // 将大写字母转换为小写字母
  inputString = inputString.toLowerCase();
  // 使用正则表达式替换非数字和小写字母字符为"-"
  return inputString.replace(/[^0-9a-z]/g, '-');
}
function randomString() {
  let str = '';
  for (let i = 0; i < 16; i++) {
    str += String.fromCharCode(Math.floor(Math.random() * 26) + 97);
  }
  return str;
}
const defaultValue = randomString();
const Bind_old = () => {
  const email = localStorage.getItem('email') || '';
  const { run } = useRequest(
    (params) =>
      request.post('/api/login/bind', {
        email: params.email,
        tenantName: params.tenantName,
        tenantCode: params.tenantCode,
      }),
    {
      manual: true,
      onSuccess: (res: any) => {
        localStorage.setItem('token', res.accessToken);
        if (res.accessToken) {
          message.success('bind success');
          setTimeout(() => {
            window.location.href = `/`;
          }, 200);
        }
      },
    },
  );
  const iptRef = useRef(null);
  const nameIptRef = useRef(null);
  const [value, setValue] = useState('');
  const [companyName, setCompanyName] = useState('');

  useEffect(() => {
    setValue(replaceNonAlphanumeric(companyName));
  }, [companyName]);
  return (
    <div>
      <div className={'w-[360px] m-auto grid gap-2 pt-10'}>
        <p className={'text-center text-lg'} style={{ fontWeight: 'bolder' }}>
          Bind Tenant
        </p>

        <Form layout={'vertical'}>
          <Form.Item
            label={'Email'}
            required
            tooltip={'Email address for email verification'}
          >
            <Input value={email} disabled={true} />
          </Form.Item>

          <Form.Item label={'Company Name'} required tooltip={'For display'}>
            <Input
              placeholder={'Please enter your company name'}
              value={companyName}
              onChange={(v) => {
                setCompanyName(v.target.value);
              }}
            />
          </Form.Item>

          <Form.Item
            label={'Company Code'}
            required
            tooltip={
              'Auto-generation with modification support for secondary domain names'
            }
          >
            {' '}
            <Input
              placeholder={'Please enter your company code'}
              value={value}
              onChange={(v) => {
                setValue(v.target.value);
              }}
            />
          </Form.Item>
          <Form.Item label={'Generated service address'}>
            <Input value={`https://${value}.arextest.com`} readOnly />
          </Form.Item>
          <div></div>

          <Form.Item>
            <Button
              className={'w-full'}
              type={'primary'}
              onClick={() => {
                localStorage.removeItem('token');
                run({
                  email,
                  tenantCode: value,
                  tenantName: companyName,
                });
              }}
            >
              Confirm
            </Button>
          </Form.Item>
        </Form>

        {/*<div>*/}
        {/*  {value && (*/}
        {/*    <div>*/}
        {/*      <span>https://{value}.arextest.com</span>*/}
        {/*    </div>*/}
        {/*  )}*/}
        {/*</div>*/}
      </div>
    </div>
  );
};

export default Bind_old;
