import { useRequest } from 'ahooks';
import { FormProps, message } from 'antd';
import { Button, Checkbox, Form, Input } from 'antd';
import React from 'react';

import request from '../../helpers/request.ts';

type FieldType = {
  email?: string;
  tenantName?: string;
  tenantCode?: string;
};

const onFinishFailed: FormProps<FieldType>['onFinishFailed'] = (errorInfo) => {
  console.log('Failed:', errorInfo);
};
function replaceNonAlphanumeric(inputString) {
  // 将大写字母转换为小写字母
  inputString = inputString.toLowerCase();
  // 使用正则表达式替换非数字和小写字母字符为"-"
  return inputString.replace(/[^0-9a-z]/g, '-');
}
const App: React.FC = () => {
  const [form] = Form.useForm<FieldType>();
  const email = localStorage.getItem('email') || '';
  form.setFieldValue('email', email);
  const { run, loading } = useRequest(
    (params) =>
      request.post('/api/login/bind', {
        email: email,
        tenantName: params.tenantName,
        tenantCode: params.tenantCode,
      }),
    {
      manual: true,
      onSuccess: (res: any) => {
        localStorage.setItem('token', res.accessToken);
        if (res.accessToken) {
          message.success('bind success');
          setTimeout(() => {
            window.location.href = `/`;
          }, 200);
        }
      },
    },
  );
  const onFinish: FormProps<FieldType>['onFinish'] = (values) => {
    console.log('Success:', values);
    run({
      // email: email,
      tenantName: values.tenantName,
      tenantCode: values.tenantCode,
    });
  };
  return (
    <div className={'m-auto'}>
      <div className={'w-[380px] m-auto text-center'}>
        <h2 className={'mt-20'}>Bind Company</h2>
        <Form
          form={form}
          name="basic"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          layout={'vertical'}
        >
          <Form.Item<FieldType>
            label="Email"
            name="email"
            required
            tooltip={'Email address for email verification'}
            rules={[
              { required: true, message: 'Please input your email!' },
              {
                type: 'email',
                message: 'The input is not valid E-mail!',
              },
            ]}
          >
            <Input disabled />
          </Form.Item>

          <Form.Item<FieldType>
            label="Company Name"
            name="tenantName"
            required
            tooltip={'For display'}
            rules={[
              { required: true, message: 'Please input your Company Name!' },
            ]}
          >
            <Input
              onChange={(v) => {
                // console.log('???');
                form.setFieldValue(
                  'tenantCode',
                  replaceNonAlphanumeric(v.target.value),
                );
              }}
            />
          </Form.Item>

          <Form.Item<FieldType>
            rules={[
              { required: true, message: 'Please input your Company Code!' },
              {
                validator: async (_, value) => {
                  if (value && !/^[a-zA-Z0-9_-]{1,20}$/.test(value)) {
                    return Promise.reject(
                      'Company Code must be 1-20 characters long and can only contain letters, numbers, underscores, and hyphens',
                    );
                  }
                },
              },
            ]}
            label="Company Code"
            name="tenantCode"
            required
            tooltip={
              'Auto-generation with modification support for secondary domain names'
            }
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button
              loading={loading}
              type="primary"
              htmlType="submit"
              className={'w-full'}
            >
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default App;
