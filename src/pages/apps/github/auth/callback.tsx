import { useRequest } from 'ahooks';
import { message } from 'antd';

import request from '../../../../helpers/request.ts';
import {useSearchParams} from "react-router-dom";

const AppsGitlabAuthCallback = () => {
  const client_id = `0440a53cf60be5c587c7`;
  const redirect_uri = `http://localhost:8000/apps/github/auth/callback`;
  const [URLSearchParams] = useSearchParams();
  const { run } = useRequest(
    () =>
      request.post('/api/oauth/token', {
        code: URLSearchParams.get('code'),
        redirectUri: redirect_uri,
      }),
    {
      manual: true,
      onSuccess: () => {
        message.success('login success');
      },
    },
  );
  useEffect(()=>{
    run()
  },[])
  return <div>AppsGitlabAuthCallback</div>;
};

export default AppsGitlabAuthCallback;
