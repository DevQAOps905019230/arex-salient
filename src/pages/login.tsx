import { useRequest } from 'ahooks';
import { message } from 'antd';
import { useNavigate } from 'react-router-dom';

import AuthWrap from '../components/AuthWrap.tsx';
import request from '../helpers/request.ts';
const onFinishFailed = (errorInfo: any) => {
  console.log('Failed:', errorInfo);
};

type FieldType = {
  username?: string;
  password?: string;
};
const Login = () => {
  const { run } = useRequest(
    (params) =>
      request.post('/api/login/login', {
        email: params.username,
        password: params.password,
      }),
    {
      manual: true,
      onSuccess: (res: any) => {
        message.success('login success');
        localStorage.setItem('token', res.accessToken);
        window.location.href = `/`;
      },
    },
  );
  const onFinish = (values: any) => {
    console.log('Success:', values);
    run(values);
  };
  return (
    <div>
      <AuthWrap
        type={'login'}
        content={
          <div>
            <Form
              name="basic"
              layout={'vertical'}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
              style={{ maxWidth: 384 }}
            >
              <Form.Item<FieldType>
                name="username"
                rules={[
                  { required: true, message: 'Please input your email!' },
                ]}
              >
                <Input placeholder={'Please input your email'} />
              </Form.Item>

              <Form.Item<FieldType>
                name="password"
                rules={[
                  { required: true, message: 'Please input your password!' },
                ]}
              >
                <Input.Password placeholder={'Please input your password'} />
              </Form.Item>

              <Form.Item>
                <Button type="primary" htmlType="submit" className={'w-[100%]'}>
                  Login
                </Button>
              </Form.Item>
            </Form>
          </div>
        }
      />
    </div>
  );
};

export default Login;
