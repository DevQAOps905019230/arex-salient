import { useRequest } from 'ahooks';
import { message } from 'antd';
import { useNavigate } from 'react-router-dom';

import AuthWrap from '../components/AuthWrap.tsx';
import request from '../helpers/request.ts';
const onFinishFailed = (errorInfo: any) => {
  console.log('Failed:', errorInfo);
};

type FieldType = {
  password?: string;
  email?: string;
};
const Signup = () => {
  const nav = useNavigate();
  const { run, loading } = useRequest(
    (params) => request.post(`/api/login/register`, params),
    {
      manual: true,
      onSuccess: () => {
        message.success('Signup successful');
        nav('/auth/wait');
      },
    },
  );
  const onFinish = (values: any) => {
    console.log('Success:', values);
    run(values);
    localStorage.setItem('email', values.email);
  };
  return (
    <div>
      <AuthWrap
        type={'signup'}
        content={
          <div>
            <Form
              name="basic"
              layout={'vertical'}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item<FieldType>
                name="email"
                rules={[
                  { required: true, message: 'Please input your email!' },
                  {
                    type: 'email',
                    message: 'The input is not valid E-mail!',
                  },
                ]}
              >
                <Input placeholder={'Please input your email'} />
              </Form.Item>

              <Form.Item<FieldType>
                name="password"
                rules={[
                  { required: true, message: 'Please input your password!' },
                ]}
              >
                <Input.Password placeholder={'Please input your password'} />
              </Form.Item>

              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className={'w-[100%]'}
                  loading={loading}
                >
                  Register Now
                </Button>
              </Form.Item>
            </Form>
          </div>
        }
      />
    </div>
  );
};

export default Signup;
