import { ArrowRightOutlined, DownloadOutlined } from '@ant-design/icons';
import { useRequest } from 'ahooks';
import {
  Card,
  Descriptions,
  DescriptionsProps,
  Progress,
  Space,
  Spin,
} from 'antd';
import dayjs from 'dayjs';

import request from '../../helpers/request.ts';
import { percent, twoNumbers } from '../../helpers/utils.ts';

const items: DescriptionsProps['items'] = [
  {
    key: '3',
    label: 'Tenant Token',
    children: (
      <div className={''}>{localStorage.getItem('tenantToken') || ''}</div>
    ),
  },
];
const Dashboard = () => {
  const { data: downloadLink, loading: loadingdownloadLink } = useRequest(() =>
    request
      .post(`/api/client/queryClientInfo`)
      .then((res: any) => res.browserDownloadUrl),
  );

  const { data, loading: loadingqueryTenant } = useRequest(() =>
    request.post(`/api/login/queryTenant`),
  );
  const { data: queryUsagerequestData, loading: loadingqueryUsage } =
    useRequest(() =>
      request
        .post(`/api/subscribe/queryUsage`, {
          tenantCode: localStorage.getItem('tenantCode'),
        })
        .then((res) => {
          return res;
        }),
    );
  const loading =
    loadingqueryTenant || loadingqueryUsage || loadingdownloadLink;
  return (
    <Spin spinning={loading}>
      <div className={'w-[1150px] pt-10 flex-col flex gap-10 pb-20'}>
        <div className={'gap-3 flex flex-col'}>
          <h1>Subscription</h1>
          <Card>
            <h2>Free Plan</h2>
            <p>
              Exp:{' '}
              {data?.user?.expireTime
                ? dayjs(data?.user?.expireTime).format('YYYY-MM-DD')
                : null}
            </p>
          </Card>

          <Card>
            <h2>Traffic Limit</h2>
            <Progress
              percent={
                queryUsagerequestData
                  ? percent(
                      queryUsagerequestData?.trafficUsageBytes,
                      queryUsagerequestData?.trafficLimitBytes,
                    )
                  : 0
              }
              showInfo={false}
            />
            <div className={'flex justify-between'}>
              <div>
                <p style={{ fontWeight: 'bolder' }}>Included in Subscription</p>
                <p>
                  {twoNumbers(queryUsagerequestData?.trafficUsageBytes)}(G) of{' '}
                  {twoNumbers(queryUsagerequestData?.trafficLimitBytes)}(G)
                </p>
              </div>

              <div className={'text-right'}>
                <p style={{ fontSize: '18px' }}>
                  {twoNumbers(queryUsagerequestData?.trafficUsageBytes)}(G)
                </p>
                <p style={{ fontSize: '16px' }} className={'text-gray-600'}>
                  Total Usage
                </p>
              </div>
            </div>
          </Card>

          <Card>
            <h2>Limit On Number Of Users</h2>
            <Progress
              percent={
                queryUsagerequestData
                  ? percent(
                      queryUsagerequestData?.memberUsage,
                      queryUsagerequestData?.memberLimit,
                    )
                  : 0
              }
              showInfo={false}
            />
            <div className={'flex justify-between'}>
              <div>
                <p style={{ fontWeight: 'bolder' }}>Included in Subscription</p>
                <p>
                  {queryUsagerequestData?.memberUsage} of{' '}
                  {queryUsagerequestData?.memberLimit}
                </p>
              </div>

              <div className={'text-right'}>
                <p style={{ fontSize: '18px' }}>
                  {queryUsagerequestData?.memberUsage}
                </p>
                <p style={{ fontSize: '16px' }} className={'text-gray-600'}>
                  Total Usage
                </p>
              </div>
            </div>
          </Card>
        </div>

        <div className={'gap-3 flex flex-col'}>
          <h1>Authentication</h1>
          <Card>
            <h2>Tenant Token</h2>
            <div className={''}>
              {localStorage.getItem('tenantToken') || ''}
            </div>
          </Card>
        </div>

        <div className={'gap-3 flex flex-col'}>
          <h1>Download</h1>
          <Card>
            <h2>Download Client</h2>
            <Space>
              <a
                href={downloadLink}
                target={'_blank'}
                rel="noreferrer"
                className={'mr-5'}
              >
                {(downloadLink || '').split('/').at(-1)}
                <DownloadOutlined style={{ marginLeft: '5px' }} />
              </a>

              <a
                href="https://github.com/arextest/releases/releases"
                target={'_blank'}
                rel="noreferrer"
              >
                All versions
                <ArrowRightOutlined style={{ marginLeft: '5px' }} />
              </a>
            </Space>
          </Card>
        </div>
      </div>
    </Spin>
  );
};

export default Dashboard;
