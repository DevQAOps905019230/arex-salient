import {
  ArrowRightOutlined,
  DownloadOutlined,
  PlusOutlined,
  ProjectFilled,
} from '@ant-design/icons';
import { useRequest } from 'ahooks';
import {
  Button,
  Card,
  Descriptions,
  DescriptionsProps,
  Progress,
  Typography,
} from 'antd';
import dayjs from 'dayjs';

import request from '../../helpers/request.ts';
import { percent, twoNumbers } from '../../helpers/utils.ts';

const { Text } = Typography;

const items: DescriptionsProps['items'] = [
  {
    key: '3',
    label: 'Tenant Token',
    children: (
      <div className={''}>{localStorage.getItem('tenantToken') || ''}</div>
    ),
  },
];

const Index = () => {
  const { data: downloadLink } = useRequest(() =>
    request
      .post(`/api/client/queryClientInfo`)
      .then((res: any) => res.browserDownloadUrl),
  );

  const { data } = useRequest(() => request.post(`/api/login/queryTenant`));
  const { data: queryUsagerequestData } = useRequest(() =>
    request
      .post(`/api/subscribe/queryUsage`, {
        tenantCode: localStorage.getItem('tenantCode'),
      })
      .then((res) => {
        return res;
      }),
  );
  // /api/subscribe/queryUsagerequest
  return (
    <div
      className={
        'mx-auto grid w-full max-w-[1248px] items-center px-6 pb-2 pt-8'
      }
    >
      <div className="flex items-center justify-between gap-3 max-md-gutters:flex-col max-md-gutters:items-start">
        <div className="grid grid-cols-1 gap-2">
          <div className="flex items-center gap-3">
            <ProjectFilled className={'text-3xl'} />
            <h1 className="css-12kl7gt mb-0">Overview</h1>
          </div>
          {/*<Text type={'secondary'}>View all of the projects associated with your account.</Text>*/}
        </div>
        {/*<div className='justify-self-end'>*/}
        {/*  <Button type={'primary'}>Create a Project</Button>*/}
        {/*</div>*/}
      </div>
      <Card className={'mt-6'} bodyStyle={{ padding: 0 }}>
        {/*<Table bordered={false} dataSource={dataSource} columns={columns} />*/}
        {/*{JSON.stringify(queryUsagerequestData)}*/}
        <div>
          <div className={'h-5'}></div>
          <Text className={'text-xl ml-5'}>Organisational Information</Text>
          <Descriptions
            column={1}
            className={'bg-white p-[20px] mb-[20px]'}
            bordered
            items={items}
          />

          {/*<p className={"text-xl"}>lists</p>*/}
          {/*<div className={'grid gap-y-2 grid-cols-1'}>*/}
          {/*  {[0].map((item, index) => {*/}
          {/*    return (*/}
          {/*      <div className={'bg-white p-[20px]'} key={index}>*/}
          {/*        <div className={'flex justify-between mb-2'}>*/}
          {/*          <div>*/}
          {/*            <Text className={'text-xl mr-5'}>AREX UI</Text>*/}
          {/*            <a*/}
          {/*              href={`https://${localStorage.getItem('tenantCode') || ''}.arextest.com`}*/}
          {/*              target={'_blank'}*/}
          {/*              rel="noreferrer"*/}
          {/*            >{`https://${localStorage.getItem('tenantCode') || ''}.arextest.com`}</a>*/}
          {/*          </div>*/}
          {/*          /!*<Button type={'primary'}>Operate</Button>*!/*/}
          {/*        </div>*/}

          {/*        /!*<Table pagination={false} dataSource={dataSource} columns={columns} />*!/*/}
          {/*      </div>*/}
          {/*    );*/}
          {/*  })}*/}
          {/*</div>*/}

          <div className={'ml-5 flex gap-5 items-center mb-5'}>
            <Text className={'text-xl'}>Download Client</Text>
            <a href={downloadLink} target={'_blank'} rel="noreferrer">
              {(downloadLink || '').split('/').at(-1)}
              <DownloadOutlined style={{ marginLeft: '5px' }} />
            </a>

            <a
              href="https://github.com/arextest/releases/releases"
              target={'_blank'}
              rel="noreferrer"
            >
              All versions
              <ArrowRightOutlined style={{ marginLeft: '5px' }} />
            </a>
          </div>

          <Text className={'text-xl ml-5'}>Expiry Date</Text>
          <span className={'ml-5'} style={{ fontSize: '16px' }}>
            {data?.user?.expireTime
              ? dayjs(data?.user?.expireTime).format('YYYY-MM-DD')
              : null}
          </span>

          <div className={'h-5'}></div>

          <Text className={'text-xl ml-5'}>Limit On Number Of Users</Text>

          <span className={'ml-5'} style={{ fontSize: '16px' }}>
            {queryUsagerequestData?.memberUsage}/
            {queryUsagerequestData?.memberLimit}
            <a href="/members" className={'ml-3'}>
              <Button type={'primary'} size={'small'}>
                <PlusOutlined />
                Add
              </Button>
            </a>
          </span>
          <div className={'h-5'}></div>

          <Text className={'text-xl ml-5'}>Traffic Limit</Text>

          <span className={'ml-5'} style={{ fontSize: '16px' }}>
            {queryUsagerequestData ? (
              <>
                {twoNumbers(queryUsagerequestData?.trafficUsageBytes)}/
                {twoNumbers(queryUsagerequestData?.trafficLimitBytes)}
                (G)
              </>
            ) : (
              <>0/0(G)</>
            )}

            <Progress
              className={'w-[500px] ml-2'}
              percent={
                queryUsagerequestData
                  ? percent(
                      queryUsagerequestData?.trafficUsageBytes,
                      queryUsagerequestData?.trafficLimitBytes,
                    )
                  : 0
              }
            />
          </span>

          <div className={'h-5'}></div>
        </div>
      </Card>
    </div>
  );
};

export default Index;
