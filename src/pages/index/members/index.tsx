import { PlusOutlined } from '@ant-design/icons';
import { useRequest } from 'ahooks';
import { Button, Drawer, message, Tooltip } from 'antd';

import request from '../../../helpers/request.ts';



const Members = () => {
  const [visible, setVisible] = useState(false);
  const [form] = Form.useForm();

  // const data = [{ email: 'tzhangm@trip.com' }]
  const {
    data,
    loading,
    run: runList,
  } = useRequest(
    () =>
      request
        .post(`/api/user/mgnt/queryUserEmails`, {
          tenantCode: localStorage.getItem('tenantCode'),
          // emails: emails,
        })
        .then((res) => res.userEmails || [])
        .then((res) => res.map((email) => ({ email }))),
    {
      // manual: true,
      onSuccess() {
        // console.log(res);
        // message.success('add success');
      },
      onError() {
        // message.error('failed');
      },
    },
  );

  const { run, loading: loadingAdd } = useRequest(
    ({ emails }) =>
      request.post(`/api/user/mgnt/addUser`, {
        tenantCode: localStorage.getItem('tenantCode'),
        emails: emails,
      }),
    {
      manual: true,
      onSuccess() {
        message.success('add success');
        runList();
      },
      onError() {
        message.error('add failed');
      },
    },
  );

  const { run: runRemove, loading: loadingRemove } = useRequest(
    ({ emails }) =>
      request.post(`/api/user/mgnt/removeUser`, {
        tenantCode: localStorage.getItem('tenantCode'),
        emails: emails,
      }),
    {
      manual: true,
      onSuccess() {
        message.success('remove success');
        runList();
      },
      onError() {
        message.error('remove failed');
      },
    },
  );

  function onFinish(values) {
    console.log(values);
    run({
      emails: values.emails,
    });
  }
  function showDrawer() {
    setVisible(true);
    form.setFieldsValue({
      emails: [],
    });
  }
  function closeDrawer() {
    setVisible(false);
  }
  const columns = [
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Operation',
      render: (text, record) => {
        return (
          <Popconfirm
            title="Delete the project"
            description="Are you sure to delete this project?"
            onConfirm={() => {
              console.log('confirm');
              // runRemove({
              //
              // })
              runRemove({
                emails: [record.email],
              });
            }}
            onCancel={() => {
              console.log('cancel');
            }}
            okText="Yes"
            cancelText="No"
          >
            <Button loading={loadingRemove} danger>
              {'Delete'}
            </Button>
          </Popconfirm>
        );
      },
    },
  ];
  return (
    <div className={'bg-white p-2'}>
      <Button type={'primary'} onClick={showDrawer}>
        <PlusOutlined />
        Add
      </Button>
      <div className={'h-2'}></div>
      <Table dataSource={data} columns={columns} loading={loading} />
      <Drawer
        title="Add User"
        open={visible}
        width={'45%'}
        onClose={closeDrawer}
      >
        <Form
          form={form}
          name="wrap"
          // labelCol={{ flex: '110px' }}
          // labelAlign="left"
          // labelWrap
          // wrapperCol={{ flex: 1 }}
          // colon={false}
          // style={{ maxWidth: 600 }}
          onFinish={onFinish}
          layout={'vertical'}
        >
          {/*<Form.Item label="tenantCode" name="tenantCode" rules={[{ required: true }]}>*/}
          {/*  <Input />*/}
          {/*</Form.Item>*/}

          <Form.Item label="Email" name={'emails'}>
            <Select
              mode="tags"
              style={{ width: '100%' }}
              placeholder="Tags Mode"
              // onChange={handleChange}
              options={[]}
            />
          </Form.Item>

          <Form.Item label=" ">
            <Button type="primary" htmlType="submit" loading={loadingAdd}>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Drawer>
    </div>
  );
};

export default Members;
